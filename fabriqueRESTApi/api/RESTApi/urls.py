from django.contrib import admin
from django.urls import re_path
from django.urls import path, include
from django.views.generic import TemplateView
from drf_yasg import openapi
from rest_framework import permissions
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title="📚FabriqueMessageAPI",
        default_version='v1.0',
        description="Тестовое api для взаимодействия с https://probe.fbrq.cloud/",
        contact=openapi.Contact(name='Shatura Nickolay', email="null.tesla@gmail.com"),
    ),
    patterns=[path('api/v1/', include('messageService.urls')), ],
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('api/v1/docs/', TemplateView.as_view(template_name='docs/main_docs.html',
                                              extra_context={'schema_url': 'openapi-schema'}), name='docs'),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),

    path("admin/", admin.site.urls),
    path("api/v1/", include('messageService.urls')),
]
