"""
APITerminal Api Requests
Здесь реализованы все запросы для работы с api
"""
import requests
import json

from . import text as api_text


def request_error(request, mode=None):
    if mode is None:
        print('Ошибка добавления.\n'
              f'Код: {request.status_code}\n'
              f'Ошибка:{request.text}')
        return 0
    else:
        print(f'Ошибка удаления: {request.status_code}/{request.text}')
        return 0


def json_objects(get_url):
    get_objects = requests.get(get_url).json()
    get_objects = json.dumps(get_objects)
    get_objects = json.loads(get_objects)
    return get_objects


def id_parser(object_id, obtain_url):

    if object_id is None:
        request_get_object = requests.get(obtain_url).json()
        for data in range(len(request_get_object)):
            print(request_get_object[data])
    else:
        request_get_object = json_objects(obtain_url)
        for data in range(len(request_get_object)):
            try:
                if object_id == request_get_object['id']:
                    return request_get_object
                else:
                    pass
            except KeyError:
                pass


'''
///////////////GET METHODS    
'''


def get_tags(obtain_tags, name=None):
    request_get_tag = json_objects(obtain_tags)

    if name is None:
        for tag in range(len(request_get_tag)):
            print(request_get_tag[tag]['id'], request_get_tag[tag]['tag_name'])
    else:
        for tag in range(len(request_get_tag)):
            if name == request_get_tag[tag]['tag_name']:
                return request_get_tag[tag]['id']
            else:
                pass


def get_operator(obtain_operator, number=None):
    request_get_operator = json_objects(obtain_operator)

    if number is None:
        for operator in range(len(request_get_operator)):
            print(request_get_operator[operator]['id'], request_get_operator[operator]['number'])
    else:
        for operator in range(len(request_get_operator)):
            if number == request_get_operator[operator]['number']:
                return request_get_operator[operator]['id']
            else:
                pass


def get_all_clients(obtain_client, client_id=None):
    return id_parser(client_id, obtain_client)


def get_all_distribution(obtain_distribution, distribution_id=None):
    return id_parser(distribution_id, obtain_distribution)


def get_all_message(obtain_messages):
    request_get_messages = requests.get(obtain_messages).json()
    for message in range(len(request_get_messages)):
        print(request_get_messages[message])


'''
///////////////POST METHODS
'''


def post_insert(insert_url, insert_object):

    insert = requests.post(insert_url, json=insert_object)

    if insert.status_code == 201:
        print(f'Успешно создана запись: {insert.text}')
    elif insert.status_code == 400:
        request_error(insert)
    else:
        request_error(insert)


'''
////////CREATE
'''


def post_create_client(client_create_url, mode):
    if mode == 'one-line':
        print('Введите в значения {телефон тэг пояс} через пробел (79006002010 bitcoin MSK):')
        try:
            phone, tag, time_zone = map(str, input().split())
        except ValueError:
            print('Не достаточно параметров/Параметров больше чем три.')
            return 0
    else:
        phone = input('Номер телефона(79006002010): ')
        tag = input('Тэг: ')
        time_zone = input('Пояс: ')

    msgobj = {'phone_number': f'{phone}',
              'tag': f'{tag}',
              'time_zone': f'{time_zone}'}

    post_insert(client_create_url, msgobj)


def post_create_distribution(distribution_create_url, all_tags, all_operator):
    date_time_start = input('Введите дату и время начала рассылки(2077-12-12T12:55:00):')
    message_text = input('Введите текст сообщения: ')
    date_time_end = input('Введите дату и время конца рассылки(2077-12-12T12:55:00):')
    tag = input('Введите тэг из существющих: ')
    operator_number = input('Введите код оператора из существющих(999):')

    tag = get_tags(all_tags, tag)
    operator_number = get_operator(all_operator, operator_number)
    msgobj = {'date_time_start': f'{date_time_start}',
              'message_text': f'{message_text}',
              'date_time_end': f'{date_time_end}',
              'tag': [tag],
              'operator_number': [operator_number]}

    post_insert(distribution_create_url, msgobj)


'''
///////////////PATCH METHODS
'''


def patch_insert(patch_url, patch_object):
    print(patch_object)
    insert = requests.patch(patch_url, json=patch_object)

    if insert.status_code == 200:
        print(f'Успешно обновлена запись: {insert.text}')
    elif insert.status_code == 400:
        request_error(insert)
    else:
        request_error(insert)


def update_client(patch_url, client_id):
    if get_all_clients(patch_url, client_id) is None:
        print('Такого клиента нет.')
        return 0
    else:
        patch_object = get_all_clients(patch_url, client_id)
        choice_list = ['1', '2', '3']
        print(f'Какой параметр хотите изменить в Client:{client_id}?')
        choice = input(f'{api_text.update_client}\n>')
        if choice in choice_list:
            if choice == '1':
                phone = input('Номер телефона> ')
                patch_object['phone_number'] = phone
            elif choice == '2':
                tag = input('Тэг> ')
                patch_object['tag'] = tag
            elif choice == '3':
                time_zone = input('Пояс> ')
                patch_object['time_zone'] = time_zone
            patch_insert(patch_url, patch_object)
        else:
            print('Такого значения нет.')
            return 0


def update_distribution(patch_url, distribution_id):
    if get_all_distribution(patch_url, distribution_id) is None:
        print('Такой рассылки нет.')
        return 0
    else:
        patch_object = get_all_distribution(patch_url, distribution_id)
        choice_list = ['1', '2', '3', '4', '5']
        print(f'Какой параметр хотите изменить в Distribution:{distribution_id}?')
        choice = input(f'{api_text.update_distribution}\n>')
        if choice in choice_list:
            if choice == '1':
                date_time_start = input('Дата и время Начала> ')
                patch_object['date_time_start'] = date_time_start
            elif choice == '2':
                message_text = input('Сообщение> ')
                patch_object['message_text'] = message_text
            elif choice == '3':
                date_time_end = input('Дата и время Конца> ')
                patch_object['date_time_end'] = date_time_end
            elif choice == '4':
                tag = input('Тэг> ')
                tag = get_tags(tag)
                patch_object['tag'] = [tag]
            elif choice == '5':
                operator_number = input('Оперптор> ')
                operator_number = get_operator(operator_number)
                patch_object['operator_number'] = [operator_number]
            patch_insert(patch_url, patch_object)
        else:
            print('Такого значения нет.')
            return 0


'''
///////////////DELETE METHODS
'''


def insert_delete(delete_url, delete_object):
    insert = requests.delete(delete_url, json=delete_object)

    if insert.status_code == 204:
        print(f'Успешно удалена запись.')
    elif insert.status_code == 400:
        request_error(insert, 1)
    else:
        request_error(insert, 1)


def delete_client(delete_url, client_id):
    if get_all_clients(delete_url, client_id) is None:
        print('Такого клиента нет.')
        return 0
    else:
        delete_object = get_all_clients(delete_url, client_id)
        insert_delete(delete_url, delete_object)


def delete_distribution(delete_url, distribution_id):
    if get_all_distribution(delete_url, distribution_id) is None:
        print('Такой рассылки нет.')
        return 0
    else:
        delete_object = get_all_distribution(delete_url, distribution_id)
        insert_delete(delete_url, delete_object)
