"""
APITerminal Static Functions
Статичные функции приветствия, проверки работы Django, хэлпер,
выход из терминала, создание записей, редактирование и удаление.
magic() - вызывает из модуля text првиетствие
exiting() - вызывает из модуля text запись конца работы терминала
check_connection() - проверяет работает ли Django или нет, по средству GET запроса на хост из настроек .env
create() - создание Клиента или Рассылки
update() - обнвление записи Клиента или Рассылки по id
delete() - удаление записи Клиента или Расслки по id
helper() - подсказки по взаимодейтсвию с терминалом.
"""

import sys
import urllib.request
from urllib.error import HTTPError, URLError

import urllib3.exceptions

from . import api_request
from . import text

protocol = ''
host = ''
port = ''
url = ''


def magic():
    print(text.hello, end='')


def exiting():
    print(text.terminal_closed)


def check_connection(protocol_local, host_local, port_local):
    global protocol
    global host
    global port
    global url
    protocol = protocol_local
    host = host_local
    port = port_local
    url = f"{protocol}://{host}:{port}/"
    try:
        urllib.request.urlopen(f"{protocol}://{host}:{port}/").getcode()
    except HTTPError:
        pass
    except (ConnectionRefusedError, URLError, urllib3.exceptions.ProtocolError):
        print(f'''Ошибка подключения к API. Пробую подключится -> {protocol}://{host}:{port}\n'''
              '''Возможна ошибка в настройке терминала, попробуте настроить .env файл.\n'''
              '''Или подключение было нарушено.''')
        sys.exit(0)


def create(self):
    global url
    client_create_url = url + '/api/v1/client/create'
    distribution_create_url = url + '/api/v1/distribution/create'
    methods = ['client', 'distribution']
    split = self.split()
    if 'client' in methods and 'client' in split:
        check_connection(protocol, host, port)
        print('Client create:')
        if '-ol' in split:
            api_request.post_create_client(client_create_url, 'one-line')
        else:
            api_request.post_create_client(client_create_url, '')

    elif 'distribution' in methods and 'distribution' in split:
        obtain_tags = url + '/api/v1/tags/'
        obtain_operator = url + '/api/v1/operator/'
        check_connection(protocol, host, port)
        print('Distribution create:')
        api_request.post_create_distribution(distribution_create_url, obtain_tags, obtain_operator)

    elif 'create' in self.strip() and len(split) == 1:
        print('Введите параметры, все параметры в команде help.')
    else:
        print('Такого метода нет.')


def show_all(self):
    global url
    methods = ['client', 'distribution', 'tag', 'operator', 'message']
    split = self.split()
    if 'client' in methods and 'client' in split:
        obtain_operator = url + '/api/v1/client/all'
        check_connection(protocol, host, port)
        api_request.get_all_clients(obtain_operator)

    elif 'distribution' in methods and 'distribution' in split:
        obtain_distribution = url + '/api/v1/distribution/all'
        check_connection(protocol, host, port)
        api_request.get_all_distribution(obtain_distribution)

    elif 'tag' in methods and 'tag' in split:
        obtain_tags = url + '/api/v1/tags/'
        check_connection(protocol, host, port)
        api_request.get_tags(obtain_tags)

    elif 'operator' in methods and 'operator' in split:
        obtain_operator = url + '/api/v1/operator/'
        check_connection(protocol, host, port)
        api_request.get_operator(obtain_operator)

    elif 'message' in methods and 'message' in split:
        obtain_messages = url + '/api/v1/messages/'
        check_connection(protocol, host, port)
        api_request.get_all_message(obtain_messages)

    elif 'all' in self.strip() and len(split) == 1:
        print('Введите параметры, все параметры в команде help.')
    else:
        print('Такого метода нет.')


def check_id(self):
    split = self.split()
    try:
        split_id = split[2]
        split_id = int(split_id)
        return split_id, split
    except ValueError:
        print('Индекс введен не верно.')
        return 0
    except IndexError:
        print('Не был введен id.')
        return 0


def edit(self, mode):
    methods = ['client', 'distribution']

    try:
        split_id = check_id(self)[0]
        split = check_id(self)[1]
    except IndexError:
        return 0
    except TypeError:
        return 0

    if mode in 'update':
        if 'client' in methods and 'client' in split:
            check_connection(protocol, host, port)
            patch_url = url + f'api/v1/client/edit/{split_id}'
            api_request.update_client(patch_url, split_id)

        elif 'distribution' in methods and 'distribution' in split:
            check_connection(protocol, host, port)
            patch_url = url + f'/api/v1/distribution/edit/{split_id}/'
            api_request.update_distribution(patch_url, split_id)

        elif 'update' in self.strip() and len(split) == 1:
            print('Введите параметры, все параметры в команде help.')
        else:
            print('Такого метода нет.')

    elif mode in 'delete':
        if 'client' in methods and 'client' in split:
            check_connection(protocol, host, port)
            delete_url = url + f'/api/v1/client/edit/{split_id}'
            api_request.delete_client(delete_url, split_id)

        elif 'distribution' in methods and 'distribution' in split:
            check_connection(protocol, host, port)
            delete_url = url + f'/api/v1/distribution/edit/{split_id}'
            api_request.delete_distribution(delete_url, split_id)

        elif 'update' in self.strip() and len(split) == 1:
            print('Введите параметры, все параметры в команде help.')
        else:
            print('Такого метода нет.')


def helper(self):
    try:
        command_input = self.split()
        if len(command_input) > 1:
            if '--client' in command_input or '-c' in command_input:
                print(text.clients)
            elif '--distribution' in command_input or '-d' in command_input:
                print(text.distribution)
            else:
                print(f'Команды "{self}", нет, попробуйте выполнить команду help.')
        else:
            print(text.helper)
    except IndexError:
        print(text.helper)
