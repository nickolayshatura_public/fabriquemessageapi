"""
APITerminal Text Variables
Текст для терминала. Пояснения, хэлпер, методы.
"""

hello = '''
       ___   ___  ____  ______              _           __
      / _ | / _ \/  _/ /_  __/__ ______ _  (_)__  ___ _/ /
     / __ |/ ___// /    / / / -_) __/  ' \/ / _ \/ _ `/ / 
    /_/ |_/_/  /___/   /_/  \__/_/ /_/_/_/_/_//_/\_,_/_/  

    Version 1.0 ------------------------ Shatura Nickolay

api Shell:
'''

terminal_closed = '''------------------ Terminal closed ------------------'''

create = '''

'''

models = '''Рассылка - Distribution,
Клиент - Client,
Сообщение - Message,
Тэги - Tags,
Номер оператора - Operator.
'''

clients = '''Clients методы: 
create client -flag - создание пользователя (flag -ol: one-line, flag {empty}: field by field)
all client - выведет список всех клиентов
update client id- обновить по id те или иные параметры 
delete client id - удалить запись по id
'''

distribution = '''Distribution методы:
create distribution - создание рассылки
all - выведет список всех рассылок
update - обновить по id те или иные параметры 
delete - удалить запись по id
'''

helper = '''
timezones - для просмтра всех временных зон
models - для просмотра всех моделей
clear - очистить экран
exit - для выхода или нажмите Ctrl+C
create model - создать запись (create client или distribution)
all model - все записи (all client, distribution, tag, operator)
update model id - редактирование записей по id (update client или distribution id)
delete model id - удаление записей по id (delete client или distribution id)


Чтобы прочитать о редактировании записей о Клиентах или Рассылке:
--client, -c - Все о методах работы с клиентами.
--distribution, -d - Все о методах работы с рассылкой.

Или используйте {ваш_хост}/api/v1/docs/.
'''

timezones = ''''EET' - Калининград
'MSK' - Москва
'SAMT' - Самара
'YEKT' - Екатеринбрг
'OMST' - Омск
'KRAT' - Красноярск
'IRKT' - Иркутск
'YAKT' - Чита
'VLAT' - Владивосток
'SAKT' - Южно-Сахалинск
'ANAT' - Анадыр
'''

update_client = '''1: Номер телефона
2: Тэг
3: Часовой пояс
'''

update_distribution = '''1: Дата и время Начала рассылки
2: Текст сообщения
3: Дата и время Конца рассылки
4: Тэг
5: Номер оператора
'''
