"""
APITerminal Commands Handler
Обработчик команд терминала, класс TerminalCore при инициализации проверяет подключение к Django,
и далее обрабатывает команды по средствам функции Command, которая принимает значения и превращает в строки.
"""
import os

from . import static
from . import text


class TerminalCore:

    def __init__(self, protocol, host, port):
        self.protocol = protocol
        self.host = host
        self.port = port
        static.check_connection(self.protocol, self.host, self.port)

    def commands(self: str):

        response = self.split()

        if 'exit' in response:
            os.system('clear')
            static.exiting()
            return 0

        elif 'help' in response:
            static.helper(self)

        elif 'timezones' in response:
            print(text.timezones)

        elif 'models' in response:
            print(text.models)

        elif 'create' in response:
            static.create(self)

        elif 'all' in response:
            static.show_all(self)

        elif 'update' in response:
            static.edit(self, 'update')

        elif 'delete' in response:
            static.edit(self, 'delete')

        elif 'clear' in response:
            os.system('clear')

        else:
            if not self.strip():
                pass
            else:
                print(f'Команды "{self}", нет, попробуйте выполнить команду help.')
