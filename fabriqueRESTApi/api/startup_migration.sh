#!/bin/bash

file="./api/RESTApi/.env"

while IFS='=' read -r key value
do
    key=$(echo $key | tr '.' '_')
    eval ${key}=\${value}
done < "$file"

name=`sed -e "s/'//" -e "s/'$//" <<<"${NAME}"`
user=`sed -e "s/'//" -e "s/'$//" <<<"${USER}"`
pass=`sed -e "s/'//" -e "s/'$//" <<<"${PASSWORD}"`

cat <<EOF >./db_init/init.sql
CREATE DATABASE ${name//[[:blank:]]/};
CREATE USER ${user//[[:blank:]]/} WITH PASSWORD '${pass//[[:blank:]]/}';
GRANT ALL PRIVILEGES ON DATABASE "${name//[[:blank:]]/}" to ${user//[[:blank:]]/};
EOF

cd  ./api

# Apply database migrations
echo "Make database migrations"
python manage.py makemigrations

# Apply database migrations
echo "Apply database migrations"
python manage.py migrate

# Start server
echo "Starting server"
python manage.py runserver 0.0.0.0:8000