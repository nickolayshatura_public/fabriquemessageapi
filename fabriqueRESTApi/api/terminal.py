"""
APITerminal
Терминал для взаимодействия с api в нем реализованы все простейшие команды для работы с базой данных.
Для помощи в работе релизвана команда help.
"""

import os
import APITerminal.commands
from RESTApi import settings


def connection_initialization():
    protocol = settings.get_from_env('protocol')
    host = settings.get_from_env('host')
    port = settings.get_from_env('port')
    return protocol, host, port


def main():
    line = APITerminal.commands
    try:
        os.system('clear')
        APITerminal.commands.TerminalCore(connection_initialization()[0], connection_initialization()[1], connection_initialization()[2])
        line.static.magic()
        while True:
            try:
                user_input = input('>')
                response = APITerminal.commands.TerminalCore.commands(user_input)
                if response == 0:
                    return 0
            except UnicodeDecodeError:
                print('Переключите раскладку.')
    except KeyboardInterrupt:
        os.system('clear')
        line.static.exiting()
        return 0


if __name__ == '__main__':
    main()


