from rest_framework import serializers
from .models import Distribution, Client, Tags, OperatorNumber, Message


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = '__all__'


class OperatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = OperatorNumber
        fields = '__all__'


class DistributionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Distribution
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['phone_number', 'tag', 'time_zone']


class ListClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
