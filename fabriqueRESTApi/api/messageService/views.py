from rest_framework import generics
from .serializer import DistributionSerializer, ClientSerializer, ListClientSerializer, TagSerializer, \
    OperatorSerializer, MessageSerializer
from .models import Client, Distribution, Tags, OperatorNumber, Message


# --------------- DISTRIBUTION ---------------


class CreateDistributionView(generics.CreateAPIView):
    serializer_class = DistributionSerializer


class AllDistributionView(generics.ListAPIView):
    serializer_class = DistributionSerializer
    queryset = Distribution.objects.all()


class EditDistributionView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DistributionSerializer
    queryset = Distribution.objects.all()


# --------------- CLIENT ---------------


class CreateClientView(generics.CreateAPIView):
    serializer_class = ClientSerializer


class AllClientsView(generics.ListAPIView):
    serializer_class = ListClientSerializer
    queryset = Client.objects.all()


class EditClientView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ListClientSerializer
    queryset = Client.objects.all()


# --------------- TAGS ---------------


class AllTagView(generics.ListAPIView):
    serializer_class = TagSerializer
    queryset = Tags.objects.all()


# --------------- OPERATOR ---------------


class AllOperatorView(generics.ListAPIView):
    serializer_class = OperatorSerializer
    queryset = OperatorNumber.objects.all()


# --------------- MESSAGE ---------------


class CreateMessageView(generics.ListAPIView):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
