from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator

tz = timezone.get_default_timezone()


class Tags(models.Model):
    tag_name = models.CharField(unique=True, max_length=30)

    def __str__(self):
        return self.tag_name


class OperatorNumber(models.Model):
    number = models.CharField(unique=True, max_length=3)

    def __str__(self):
        return self.number


class Client(models.Model):
    phone_regex = RegexValidator(regex=r'^\+?1?[7]\d{7,11}$',
                                 message="Формат номера телефона должен быть: '79999999999'.")
    phone_number = models.CharField(unique=True, validators=[phone_regex], max_length=11)
    operator_number = models.IntegerField(null=True)
    tag = models.CharField(max_length=20)
    timezones = {
        ('EET', 'Kaliningrad'),
        ('MSK', 'Moscow'),
        ('SAMT', 'Samara'),
        ('YEKT', 'Yekaterinburg'),
        ('OMST', 'Omsk'),
        ('KRAT', 'Krasnoyarsk'),
        ('IRKT', 'Irkutsk'),
        ('YAKT', 'Chita'),
        ('VLAT', 'Vladivostok'),
        ('SAKT', 'Yuzhno-Sakhalinsk'),
        ('ANAT', 'Anadyr'),
    }
    time_zone = models.CharField(max_length=9, choices=timezones)

    def save(self, *args, **kwargs):
        self.operator_number = self.phone_number[1:4]

        if Tags.objects.filter(tag_name=self.tag):
            pass
        else:
            Tags.objects.create(tag_name=self.tag)

        if OperatorNumber.objects.filter(number=self.operator_number):
            pass
        else:
            OperatorNumber.objects.create(number=self.operator_number)

        super(Client, self).save(*args, **kwargs)


class Distribution(models.Model):
    date_time_start = models.DateTimeField()
    message_text = models.CharField(max_length=100)
    tag = models.ManyToManyField(Tags)
    operator_number = models.ManyToManyField(OperatorNumber)
    date_time_end = models.DateTimeField()


class Message(models.Model):
    date_time_send = models.DateTimeField()
    PENDING = 0
    DONE = 1
    STATUSES = {
        (PENDING, 'Ожидает отправки'),
        (DONE, 'Отправлено'),
    }
    status = models.IntegerField(choices=STATUSES, default=PENDING)
    id_distribution = models.ManyToManyField(Distribution)
    id_client = models.ManyToManyField(Client)

