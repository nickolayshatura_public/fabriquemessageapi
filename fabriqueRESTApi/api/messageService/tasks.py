from celery import shared_task
import logging
import datetime
import requests
from . import models
from pytz import timezone


def send_message(data):
    send_time = datetime.datetime.now()
    clients = models.Client.objects.all()
    for client in clients:
        if client.tag == str(data.tag.all()[0]) and str(client.operator_number) == str(data.operator_number.all()[0]):
            if models.Message.objects.filter(id_distribution=data.pk):
                pass
            else:
                message = models.Message.objects.create(date_time_send=send_time, status=0)
                message_id = message.pk
                url = f'https://probe.fbrq.cloud/v1/send/{message_id}'
                msgobj = {"id": message_id,
                          "phone": int(client.phone_number),
                          "text": data.message_text
                          }
                response = requests.post(url, headers={
                    'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDE1MTExOTMsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6ImlhbW5pY2tvbGF5In0.BzwwiSvYTIFplM-K43WTjcr0_GcqR-ESRdsRQpYI07E'},
                                         timeout=10, json=msgobj)

                if response.text:
                    logging.info(f'Text of response = {response.text}')

                    try:
                        json_data = response.json()

                        if json_data:
                            logging.info(f'Json data = {json_data}')

                            try:
                                message_status = json_data['message']
                                code = json_data['code']

                                if message_status == 'OK' and code == 0:
                                    logging.info(f'Data = {message_status}')

                                    message.save()
                                    message.id_distribution.add(data.pk)
                                    message.id_client.add(client.pk)
                                    message.status = 1
                                    message.save()
                                    logging.info(client)
                                    logging.info(client.pk)
                                else:
                                    logging.info('Data is falsy.')
                                    message.delete()
                            except (KeyError, IndexError, TypeError):
                                logging.info('Data does not have the inner structure did not expect.')
                                message.delete()
                        else:
                            logging.info('Empty JSON string.')
                            message.delete()

                    except ValueError:
                        logging.info('No JSON returned.')
                        message.delete()

                else:
                    logging.info('Response body contains -  None!')
                    message.delete()


@shared_task
def check_distributions(time_zone):
    time_zone_settings = timezone(time_zone)
    today = datetime.datetime.now(time_zone_settings).timestamp()

    # logging.basicConfig(level=logging.INFO, filename=f'{today}_py_log.log', filemode='a', format="%(asctime)s %(levelname)s %(message)s")

    distributions = models.Distribution.objects.all()
    for data in distributions:
        start = data.date_time_start.astimezone(tz=time_zone_settings).timestamp()
        end = data.date_time_end.astimezone(tz=time_zone_settings).timestamp()
        if start <= today <= end:
            send_message(data)
