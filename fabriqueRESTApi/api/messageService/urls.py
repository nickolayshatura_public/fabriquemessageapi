from django.urls import path

from .views import *

app_name = 'messageService'
urlpatterns = [
    # --------------- DISTRIBUTION ---------------
    path('distribution/create', CreateDistributionView.as_view()),
    path('distribution/edit/<int:pk>/', EditDistributionView.as_view()),
    path('distribution/all', AllDistributionView.as_view()),
    # --------------- CLIENTS --------------------
    path('client/create', CreateClientView.as_view()),
    path('client/edit/<int:pk>', EditClientView.as_view()),
    path('client/all', AllClientsView.as_view()),
    # --------------- MASSAGES -------------------
    path('messages/', CreateMessageView.as_view()),
    # --------------- TAGS -----------------------
    path('tags/', AllTagView.as_view()),
    # --------------- OPERATOR -------------------
    path('operator/', AllOperatorView.as_view()),
]
